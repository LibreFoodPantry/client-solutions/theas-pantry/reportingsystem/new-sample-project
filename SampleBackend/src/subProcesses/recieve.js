const amqp = require('amqplib/callback_api');
const MongoClient = require('mongodb');

module.exports = function(app){
    //creates main async function for db
    // async function main() {

        //

        //create a connection
        amqp.connect('amqp://localhost', function (connError, connection) {
            if (connError) throw connError;

            //create channel
            connection.createChannel(function (channError, channel) {
                if (channError) throw channError;
                //assert queue
                const queue = 'test'
                channel.assertQueue(queue);

                console.log('Waiting for messages');
                //receive messages
                channel.consume(queue, function (msg) {
                    var data = JSON.parse(msg.content);
                    console.log(data);
                });
            })
        });
    // }
}