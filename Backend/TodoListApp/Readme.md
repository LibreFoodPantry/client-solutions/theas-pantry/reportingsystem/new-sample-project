#Docker commands to build and run project

###Change current directory to TodoListApp
###Build: docker build -t \<username>/node-js-playlist .
###Run in background: docker run -p 9000:3000 -d \<username>/node-js-playlist
###Type in browser \<local ip>:9000/todo
