// Austin Engel

var bodyParser = require('body-parser');
var mongoose = require('mongoose');

//connect to database
mongoose.connect('mongodb+srv://austin:mongodpassword@todo-list.alb3y.mongodb.net/todo-list?retryWrites=true&w=majority');

//create schema - blueprint for data
var todoSchema = new mongoose.Schema({
    item: String
});

//create model
var Todo = mongoose.model('Todo', todoSchema);

var urlencodedParser = bodyParser.urlencoded({ extended: false })

module.exports = function(app){

    app.get('/todo', function(req, res){
        //get data from mongodb and pass it to the view
        Todo.find({}, function(err, data){
            if(err) throw err;
            res.render('todo', {todos: data});
        });
    });

    app.post('/todo', urlencodedParser, function(req, res){
        //get data from the view and pass it to mongodb
        Todo(req.body).save((err, data)=>{
            if(err) throw err;
            res.json(data);
        });
    });

    app.delete('/todo/:item', function(req, res){
        //delete requested item from mongodb
        Todo.find({item: req.params.item.replace(/\-/g, " ")}).remove((err, data)=>{
            if(err) throw err;
            res.json(data);
        });
    });

};