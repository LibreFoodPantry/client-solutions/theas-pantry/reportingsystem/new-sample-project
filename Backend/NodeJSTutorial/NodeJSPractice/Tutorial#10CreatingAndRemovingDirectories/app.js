var fs = require('fs');


fs.mkdir('stuff', function(){
    fs.readFile('readMe.txt', 'utf8', function(err, data){
        fs.writeFile('./stuff/writeme.txt', data, function(){
            console.log('transfer is complete');
        })
    });
});

//these should be syncronous if put together
// instead of asyncronus
//these are just examples for both
fs.unlink('./stuff/writeMe.txt', function(){
    fs.rmdir('stuff', function(){
        console.log('deletion complete');
    });
});